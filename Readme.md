# Development Environment

This repository is a compendium of development environment scripts, files, and
documentation for deploying an [OSM](https://osm.etsi.org), [ONAP](https://docs.onap.org/en/latest/),
or other VNF orchestrator.

## Prerequisites

[Raven](https://gitlab.com/mergetb/tech/raven): Raven is a libvirt application
that allows us to build complex virtual network topologies.

### Raven Quick How-To

Raven uses a `model.js` which defines the topology.  It stores all of the relevent
information about the topology such as nodes, switches, and connections. There
is a helper script called `run.sh` in this repository which brings up the
raven environment, and establishes an ansible inventory file for later usage.

This development environment also requires `nested virtualization` to be enabled.
You will need to modprobe your `kvm_intel` or `kvm_amd` module with options:
`nested=1` and reloading the module.  For persistence see guide: https://ostechnix.com/how-to-enable-nested-virtualization-in-kvm-in-linux/

## Usage

```
./run.sh
ansible-playbook -i .rvn/ansible-hosts rvn-configs/osm-config.yml
ansible-playbook -i .rvn/ansible-hosts rvn-configs/osm-devstack.yml
```

Until the following commands are ansiblized:

Devstack host:
```
# open clouds.yml, you'll need this for the next step
devstack$ cat /etc/openstack/clouds.yml
```

OSM host:
```
# create a vim to point at devstack host.  The password and auth_url are stored
# in clouds.yml
osm$ osm vim-create --name devstack --user admin --password secret --auth_url http://172.22.0.183/identity --tenant admin --account_type openstack --config '{insecure: True, APIversion: v3.3}'

# verified that the vim is `ENABLED`. If you see `ERROR`, time to debug.
osm$ osm vim-list

# now lets create our vnf / ns
osm$ cp /etc/osm/fu2_*
osm$ tar -xvf fu2_vnf.tar
osm$ tar -xvf fu2_ns.tar
osm$ osm vnfpkg-create fu2_vnf
osm$ osm nspkg-create fu2_vnf
osm$ osm ns-create --vim_account devstack --ns_name fu2 --nsd_name fu2-ns --config '{vld: [ {name: mgmtnet, vim-network-name: public} ] }'
osm$ osm ns-list
```

When the state is ready, on devstack you can confirm it is running
```
devstack$ openstack --os-cloud devstack-admin server list
```
