/*
 *
 * This model is a simple 2 node topology.  With a dedicated OSM and
 * dedicated devstack (openstack on a box) installation.  The OSM
 * manages vnf/ns, while devstack manages vm and underlay networks.
 *
 */

osm = {
  'name': 'osm',
  'image': 'ubuntu-1804',
  'cpu': { 
	  'cores': 4,
  },
  'memory': { 'capacity': GB(8) },
  'disks': [ { 'size': "64G", 'dev': 'vdb', 'bus': 'virtio' } ]
};

devstack = {
  'name': 'devstack',
  'image': 'ubuntu-2004',
  'cpu': { 
	  'cores': 4,
	  'passthru': true,
  },
  'memory': { 'capacity': GB(8) },
  'disks': [ { 'size': "64G", 'dev': 'vdb', 'bus': 'virtio' } ]
};

sw = {                                                
	  'name': 'sw',                                       
	  'image': 'cumulusvx-4.1',                               
	  'os': 'linux',                                          
	  'defaultdisktype': { 'bus': 'virtio', 'dev': 'vda' },
	  'cpu': { 'cores': 1 },                                                      
	  'memory': { 'capacity': GB(2) },   
}

topo = {
  'name': 'osm',
  'nodes': [osm, devstack], //openvim
  'switches': [sw], //mgmt, overlay],
  'links': [
    Link('osm', 1, 'sw', 1),
    Link('devstack', 1, 'sw', 2),
  ]
}
