/*
 * Small-World System
 * ------------------
 *  A raven topology with an IoT device, an Android phone and a server
 *
 */

vim = {
  'name': 'vim-openvim',
  'image': 'ubuntu-1604',
  'cpu': { 'cores': 2, 'passthru': true },
  'memory': { 'capacity': GB(8) },
  'disks': [ { 'size': "48G", 'dev': 'vdb', 'bus': 'virtio' } ]
};

stack = {
  'name': 'vim-openstack',
  'image': 'ubuntu-1804',
  'cpu': { 'cores': 8  },
  'memory': { 'capacity': GB(16) },
  'disks': [ { 'size': "64G", 'dev': 'vdb', 'bus': 'virtio' } ]
};

osm = {
  'name': 'osm',
  'image': 'ubuntu-1804',
  'cpu': { 'cores': 4 },
  'memory': { 'capacity': GB(16) },
  'disks': [ { 'size': "64G", 'dev': 'vdb', 'bus': 'virtio' } ]
};

sw = {                                               
  'name': 'sw',                                      
  'image': 'cumulusvx-4.1',                              
  'os': 'linux',                                         
  'defaultdisktype': { 'bus': 'virtio', 'dev': 'vda' },
  'cpu': { 'cores': 1 },                                                     
  'memory': { 'capacity': GB(2) },   
}

mgmt = {                                               
  'name': 'mgmt',                                      
  'image': 'cumulusvx-4.1',                              
  'os': 'linux',                                         
  'defaultdisktype': { 'bus': 'virtio', 'dev': 'vda' },
  'cpu': { 'cores': 1 },                                                     
  'memory': { 'capacity': GB(2) },   
}

overlay = {                                               
  'name': 'overlay',                                      
  'image': 'cumulusvx-4.1',                              
  'os': 'linux',                                         
  'defaultdisktype': { 'bus': 'virtio', 'dev': 'vda' },
  'cpu': { 'cores': 1 },                                                     
  'memory': { 'capacity': GB(2) },   
}

topo = {
  'name': 'osm',
  'nodes': [osm, stack], //openvim
  'switches': [sw], //mgmt, overlay],
  'links': [
    //Link('vim-openvim', 1, 'sw', 1),
    //Link('vim-openvim', 2, 'mgmt', 1),
    //Link('vim-openvim', 3, 'overlay', 1),
    Link('vim-openstack', 1, 'sw', 2),
    //Link('vim-openstack', 2, 'mgmt', 2),
    //Link('vim-openstack', 3, 'overlay', 2),
    Link('osm', 1, 'sw', 3),
    //Link('osm', 1, 'mgmt', 3),
    //Link('osm', 1, 'overlay', 3),
  ]
}
