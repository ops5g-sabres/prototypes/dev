/*
 * Charmed Model
 */

osm = {
  'name': 'osm',
  'image': 'ubuntu-1804',
  'cpu': { 'cores': 8 },
  'memory': { 'capacity': GB(24) },
  'disks': [ { 'size': "128G", 'dev': 'vdb', 'bus': 'virtio' } ]
};

topo = {
  'name': 'osm',
  'nodes': [osm], //openvim
  'switches': [], //mgmt, overlay],
  'links': [
    //Link('vim-openvim', 1, 'sw', 1),
    //Link('vim-openvim', 2, 'mgmt', 1),
    //Link('vim-openvim', 3, 'overlay', 1),
    //Link('vim-openstack', 1, 'sw', 2),
    //Link('vim-openstack', 2, 'mgmt', 2),
    //Link('vim-openstack', 3, 'overlay', 2),
    //Link('osm', 1, 'sw', 3),
    //Link('osm', 1, 'mgmt', 3),
    //Link('osm', 1, 'overlay', 3),
  ]
}
