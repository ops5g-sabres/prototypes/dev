#!/usr/bin/python3

"""

See comments in convert-vnf-to-sol006.py.

tl;dr osm tooling does not generate sol006 descriptors, so this file
is necessary to convert to sol006 in release 9+.

"""

from osm_im.im_translation import translate_im_nsd_to_sol006
import unittest
import yaml
import sys


if len(sys.argv) != 2:
    print(sys.argv[0]+" <file>")
    sys.exit(1)


data = {}
with open(sys.argv[1], 'r') as reader:
    data = yaml.safe_load(reader)
    with open(sys.argv[1]+".orig", 'w') as writer:
        print(data)
        yaml.dump(data, writer)


translation = translate_im_nsd_to_sol006(data)

with open(sys.argv[1], 'w') as writer:
    print(translation)
    yaml.dump(translation, writer)
