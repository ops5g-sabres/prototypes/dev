#!/usr/bin/python3

"""
  Not clear from OSM documentation (which has 404 links) what is the appropriate
  OSM tool to create SOL0006 VNF and NS files.

  https://osm.etsi.org/docs/vnf-onboarding-guidelines/02-day0.html
  >  "NOTE: this section uses pre-SOL006 descriptors which need to be converted before trying in Release 9+"

  `osm package-create --image ubuntu16.04 --vendor OSM vnf ubu`
  will create the depricated decriptor, so this file in dedicated to translating
  that pre-sol006 defn. to sol006 using the OSM IM library.
"""

from osm_im.im_translation import translate_im_vnfd_to_sol006
import unittest
import yaml
import sys


if len(sys.argv) != 2:
    print(sys.argv[0]+" <file>")
    sys.exit(1)


data = {}
with open(sys.argv[1], 'r') as reader:
    data = yaml.safe_load(reader)
    with open(sys.argv[1]+".orig", 'w') as writer:
        print(data)
        yaml.dump(data, writer)

translation = translate_im_vnfd_to_sol006(data)

with open(sys.argv[1], 'w') as writer:
    print(translation)
    yaml.dump(translation, writer)
